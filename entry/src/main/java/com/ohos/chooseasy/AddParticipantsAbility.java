package com.ohos.chooseasy;

import java.util.ArrayList;
import java.util.List;

import com.ohos.chooseasy.utils.CustomToolbar;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

/**
 * AddParticipantsAbility
 *
 * @since 2021-06-01
 */
public class AddParticipantsAbility extends Ability implements Selection.SelectableListener {
    private ListContainer listAppProfiles;
    private List<Selection.SelectableGroup> mFavouritiesCollection;
    private MultiSelectOptionsArrayAdapter selectOptionsAdapter;
    private ProfileAutoCompleteView autoSelectProfiles;
    private int gridColumnsCount = 1;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_add_participants);
        getWindow().setStatusBarColor(Color.getIntColor("#3F51B5"));
        CustomToolbar customTpplbar = (CustomToolbar) findComponentById(ResourceTable.Id_custom_toolbar);
        customTpplbar.setTitle(ResourceTable.String_app_name);
        initView();
    }

    private void initView() {
        // Required empty public constructor
        mFavouritiesCollection = new ArrayList<>();
        Selection.SelectableGroup flyGroup = new Selection.SelectableGroup("WE FLY");
        List<Selection.SelectableItem> flyCollection = new ArrayList<>();
        flyCollection.add(new Selection.SelectableItem("Pigeon", 11));
        flyCollection.add(new Selection.SelectableItem("Parrot", 12));
        flyCollection.add(new Selection.SelectableItem("Lady Bird", 13));
        flyCollection.add(new Selection.SelectableItem("Eagle", 14));
        flyGroup.addSelectableList(flyCollection);
        mFavouritiesCollection.add(flyGroup);
        Selection.SelectableGroup swimGroup = new Selection.SelectableGroup("WE SWIM");
        List<Selection.SelectableItem> swimCollection = new ArrayList<>();
        swimCollection.add(new Selection.SelectableItem("Gold Fish", 21));
        swimCollection.add(new Selection.SelectableItem("Dolphin", 22));
        swimCollection.add(new Selection.SelectableItem("Jelly Fish", 23));
        swimCollection.add(new Selection.SelectableItem("Tortoise", 24));
        swimGroup.addSelectableList(swimCollection);
        mFavouritiesCollection.add(swimGroup);
        Selection.SelectableGroup runGroup = new Selection.SelectableGroup("WE RUN");
        List<Selection.SelectableItem> runCollection = new ArrayList<>();
        runCollection.add(new Selection.SelectableItem("Rabbit", 31));
        runCollection.add(new Selection.SelectableItem("Cat", 32));
        runCollection.add(new Selection.SelectableItem("Dog", 33));
        runCollection.add(new Selection.SelectableItem("Horse", 34));
        runGroup.addSelectableList(runCollection);
        mFavouritiesCollection.add(runGroup);

        selectOptionsAdapter = new MultiSelectOptionsArrayAdapter(this, mFavouritiesCollection,gridColumnsCount, this);

        autoSelectProfiles = (ProfileAutoCompleteView) findComponentById(ResourceTable.Id_auto_select_profiles);

        listAppProfiles = (ListContainer) findComponentById(ResourceTable.Id_list_app_profiles);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(gridColumnsCount);

        listAppProfiles.setLayoutManager(tableLayoutManager);
        listAppProfiles.setItemProvider(selectOptionsAdapter);
    }

    private void updateItemToSelectedTokens(Selection.Selectable token) {
        if (token.isSelected()) {
            autoSelectProfiles.addObject(token);
        } else {
            autoSelectProfiles.removeObject(token);
        }
    }

    @Override
    public void onSelectableClicked(Selection.Selectable selectItem) {
        updateItemToSelectedTokens(selectItem);
    }
}