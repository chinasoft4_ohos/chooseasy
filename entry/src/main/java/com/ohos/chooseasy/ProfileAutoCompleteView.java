package com.ohos.chooseasy;

import com.ohos.chooseasy.utils.FlowLayout;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * ProfileAutoCompleteView
 *
 * @since 2021-05-25
 */
public class ProfileAutoCompleteView extends FlowLayout {
    private Component parse;
    private Text name;
    private Map<Integer,Component> parseMap = null;

    /**
     * ProfileAutoCompleteView
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public ProfileAutoCompleteView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        parseMap = new HashMap<>();
    }

    /**
     * ProfileAutoCompleteView
     *
     * @param context context
     * @param attrs attrs
     */
    public ProfileAutoCompleteView(Context context, AttrSet attrs) {
        super(context, attrs);
        parseMap = new HashMap<>();
    }

    /**
     * addObject
     *
     * @param token token
     */
    public void addObject(Selection.Selectable token) {
        parse = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_chip_layout, null, false);

        name = (Text) parse.findComponentById(ResourceTable.Id_txt_user_name);
        name.setText(token.getName());
        parseMap.put(token.getId(),parse);
        addComponent(parse);
    }

    /**
     * removeObject
     *
     * @param token token
     */
    public void removeObject(Selection.Selectable token) {
        Component component = parseMap.get(token.getId());
        removeComponent(component);
    }
}