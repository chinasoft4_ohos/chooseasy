/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.chooseasy.utils;

import com.ohos.chooseasy.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * CustomToolbar
 *
 * @since 2021-04-07
 *
 */
public class CustomToolbar extends DirectionalLayout {
    /** 构造CustomToolbar
     *
     * @param context context
     */
    public CustomToolbar(Context context) {
        super(context);
        initView(context);
    }

    /** 构造CustomToolbar
     *
     * @param context context
     * @param attrSet attrSet
     */
    public CustomToolbar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    /** 构造CustomToolbar
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public CustomToolbar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    /**
     * 初始化布局
     *
     * @param context context
     */
    private void initView(Context context) {
        mContext = context;
        Component layout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_ability_toolbar_main,this,true);
        initMenu(context, layout);
    }

    /**
     * 初始化自定义的menu样式
     *
     * @param context context
     * @param layout layout
     */
    private void initMenu(Context context, Component layout) {

    }

    /** setTitle设置标题
     *
     * @param title title
     */
    public void setTitle(int title) {
        Component layout = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_toolbar_main, this, true);
        Text mainTitle = (Text) layout.findComponentById(ResourceTable.Id_main_title);
        mainTitle.setText(title);
    }

    /** setTitleSize设置标题大小
     *
     * @param titleSize titleSize
     */
    public void setTitleSize(int titleSize) {
        Component layout = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_toolbar_main, this, true);
        Text mainTitle = (Text) layout.findComponentById(ResourceTable.Id_main_title);
        mainTitle.setTextSize(titleSize);
    }

    /** getTitleSize获取标题大小
     *
     * @return  getTitleSize
     */
    public int getTitleSize() {
        Component layout = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_toolbar_main, this, true);
        Text mainTitle = (Text) layout.findComponentById(ResourceTable.Id_main_title);
        return mainTitle.getTextSize();
    }
}
