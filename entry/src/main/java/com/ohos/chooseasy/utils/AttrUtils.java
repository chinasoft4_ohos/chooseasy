/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ohos.chooseasy.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * 获取自定义属性工具类,如果没有配置这个自定义属性则使用默认值。
 *
 * @since 2021-03-01
 */
public class AttrUtils {
    private AttrUtils() {
    }

    /**
     * 获取Float资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static float getFloatValueByAttr(AttrSet attrSet, String filedName, float defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getFloatValue();
        }
        return defaultValue;
    }

    /**
     * 获取Int资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static int getIntValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getIntegerValue();
        }
        return defaultValue;
    }

    /**
     * 获取Color资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static Color getColorValueByAttr(AttrSet attrSet, String filedName, Color defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getColorValue();
        }
        return defaultValue;
    }

    /**
     * 获取Dimension资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static int getDimensionValueByAttr(AttrSet attrSet, String filedName, int defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getDimensionValue();
        }
        return defaultValue;
    }

    /**
     * 获取String资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static String getStringValueByAttr(AttrSet attrSet, String filedName, String defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getStringValue();
        }
        return defaultValue;
    }

    /**
     * 获取Boolean资源值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 资源值
     */
    public static boolean getBooleanValueByAttr(AttrSet attrSet, String filedName, boolean defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getBoolValue();
        }
        return defaultValue;
    }

    /**
     * 获取Element元素值
     *
     * @param attrSet 属性集合
     * @param filedName 属性名称
     * @param defaultValue 默认值
     * @return 元素值
     */
    public static Element getElementValueByAttr(AttrSet attrSet, String filedName, Element defaultValue) {
        Attr attr = get(attrSet, filedName);
        if (attr != null) {
            return attr.getElement();
        }
        return defaultValue;
    }

    private static Attr get(AttrSet attrSet, String filedName) {
        if (attrSet != null && attrSet.getAttr(filedName).isPresent()) {
            return attrSet.getAttr(filedName).get();
        }
        return null;
    }
}
