package com.ohos.chooseasy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Selection
 *
 * @since 2021-05-25
 */
public class Selection {
    /**
     * SelectedResult
     *
     * @since 2021-05-25
     */
    public static class SelectedResult {

        int displayViewId;
        Selectable selectedItem;

        /**
         * SelectedResult
         *
         * @param selectedItem selectedItem
         * @param displayViewId displayViewId
         */
        public SelectedResult(Selectable selectedItem, int displayViewId) {
            this.selectedItem = selectedItem;
            this.displayViewId = displayViewId;
        }

        public Selectable getSelectedItem() {
            return selectedItem;
        }

        public int getDisplayViewId() {
            return displayViewId;
        }
    }

    /**
     * SelectDisplayType
     *
     * @since 2021-05-25
     */
    public enum SelectDisplayType {
        /**
         * SelectDisplayType
         *
         * @since 2021-05-25
         */
        SINGLE(1),
        /**
         * SelectDisplayType
         *
         * @since 2021-05-25
         */
        GROUP(2);

        private final int value;

        public int getValue() {
            return value;
        }

        SelectDisplayType(int value) {
            this.value = value;
        }
    }

    /**
     * Selectable
     *
     * @since 2021-05-25
     */
    public interface Selectable {
        /**
         * Selectable
         *
         * @param selected selected
         */
        void setSelected(boolean selected);

        /**
         * Selectable
         *
         * @return isSelected
         */
        boolean isSelected();

        /**
         * Selectable
         *
         * @return getId
         */
        int getId();

        /**
         * Selectable
         *
         * @return getName
         */
        String getName();

        /**
         * Selectable
         *
         * @return getImage
         */
        String getImage();
    }

    /**
     * SelectableItem
     *
     * @since 2021-05-25
     */
    public static class SelectableItem
            implements Selectable, Serializable {
        private String name;
        private int id;
        private boolean isSelected = false;

        /**
         * SelectableItem
         *
         * @param name name
         * @param id id
         */
        public SelectableItem(String name, int id) {
            this.name = name;
            this.id = id;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }

        public String getImage() {
            return null;
        }
    }

    /**
     * SelectableGroup
     *
     * @since 2021-05-25
     */
    public static class SelectableGroup {
        private final String title;
        private final ArrayList<Selectable> collection;

        /**
         * SelectableGroup
         *
         * @param title title
         */
        public SelectableGroup(String title) {
            this.title = title;
            collection = new ArrayList<>();
        }

        /**
         * addSelectable
         *
         * @param item item
         * @return addSelectable
         */
        public boolean addSelectable(SelectableItem item) {
            return collection.add(item);
        }

        /**
         * addSelectableList
         *
         * @param list list
         * @return addSelectableList
         */
        public boolean addSelectableList(List<SelectableItem> list) {
            return collection.addAll(list);
        }

        public String getTitle() {
            return title;
        }

        public List<Selectable> getCollection() {
            return collection;
        }
    }

    /**
     * SelectableListener
     *
     * @since 2021-05-25
     */
    public interface SelectableListener {
        /**
         * SelectableListener
         *
         * @param selectItem selectItem
         */
        void onSelectableClicked(Selectable selectItem);
    }
}