package com.ohos.chooseasy;

import ohos.agp.components.*;

import java.util.List;

/**
 * SelectOptionsArrayAdapter
 *
 * @since 2021-05-25
 */
public class SelectOptionsArrayAdapter
        extends BaseItemProvider {
    private final List<Selection.Selectable> collection;
    private final Selection.SelectableListener listener;
    private final int gridColumnsCount;

    /**
     * SelectOptionsArrayAdapter
     *
     * @param listener listener
     * @param collection collection
     */
    public SelectOptionsArrayAdapter(Selection.SelectableListener listener, List<Selection.Selectable> collection) {
        this(listener, collection, 1);
    }

    /**
     * SelectOptionsArrayAdapter
     *
     * @param listener listener
     * @param collection collection
     * @param columnsCount columnsCount
     */
    public SelectOptionsArrayAdapter(Selection.SelectableListener listener, List<Selection.Selectable> collection, int columnsCount) {
        this.collection = collection;

        this.listener = listener;
        gridColumnsCount = columnsCount;
    }


    private void loadImageToView(Image view, Selection.Selectable selectItem) {
        view.setPixelMap(ResourceTable.Graphic_profile_large);
    }

    @Override
    public int getCount() {
        return collection != null ? collection.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return collection.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        int viewType = getItemViewType(i);
        SingleViewHolder viewHolder;
        GroupViewHolder viewHolder1;
        if (i == Selection.SelectDisplayType.SINGLE.getValue()
                && viewType == Selection.SelectDisplayType.SINGLE.getValue()) {
            Component parer = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector, componentContainer, false);
            viewHolder = new SingleViewHolder(parer, i);

            final Selection.Selectable selectItem = collection.get(i);
            viewHolder.selectprofiletitle.setText(selectItem.getName());
            loadImageToView(viewHolder.imgprofilemini, selectItem);

            viewHolder.rootView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    listener.onSelectableClicked(selectItem);
                }
            });
            return parer;
        } else {
            Component component1 = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector_group, componentContainer, false);
            viewHolder1 = new GroupViewHolder(component1, i);

            Selection.SelectableGroup selectGroup = (Selection.SelectableGroup) collection.get(i);
            viewHolder1.selectgrouptitle.setText(selectGroup.getTitle());
            ListContainer recyclerView = viewHolder1.lytselectgroupcollection;
            TableLayoutManager tableLayoutManager = new TableLayoutManager();
            tableLayoutManager.setColumnCount(gridColumnsCount);
            recyclerView.setLayoutManager(tableLayoutManager);
            recyclerView.setItemProvider(new SelectableArrayAdapter(selectGroup.getCollection()));
            return component1;
        }
    }

    /**
     * getItemViewType
     *
     * @param position int
     * @return getItemViewType
     */
    public int getItemViewType(int position) {
        if (collection.get(position) instanceof Selection.Selectable) {
            return Selection.SelectDisplayType.SINGLE.getValue();
        } else if (collection.get(position) instanceof Selection.SelectableGroup) {
            return Selection.SelectDisplayType.GROUP.getValue();
        } else {
            return 0;
        }
    }

    /**
     * SingleViewHolder
     *
     * @since 2021-05-25
     */
    public static class SingleViewHolder {
        /**
         * SingleViewHolder
         */
        public final Component rootView;
        /**
         * SingleViewHolder
         */
        public final CircleImageView imgprofilemini;
        /**
         * SingleViewHolder
         */
        public final Text selectprofiletitle;
        /**
         * SingleViewHolder
         *
         * @param convertView convertView
         * @param viewType viewType
         */
        public SingleViewHolder(Component convertView, int viewType) {
            super();
            rootView = convertView;
            imgprofilemini = (CircleImageView) convertView.findComponentById(ResourceTable.Id_img_profile_mini);
            selectprofiletitle = (Text) convertView.findComponentById(ResourceTable.Id_select_title);
        }
    }

    /**
     * GroupViewHolder
     *
     * @since 2021-05-25
     */
    public static class GroupViewHolder {
        /**
         * GroupViewHolder
         */
        public final ListContainer lytselectgroupcollection;
        /**
         * GroupViewHolder
         */
        public final Text selectgrouptitle;
        /**
         * GroupViewHolder
         *
         * @param convertView convertView
         * @param viewType viewType
         */
        public GroupViewHolder(Component convertView, int viewType) {
            super();
            lytselectgroupcollection = (ListContainer) convertView.findComponentById(ResourceTable.Id_lyt_select_group_collection);
            selectgrouptitle = (Text) convertView.findComponentById(ResourceTable.Id_select_group_title);
        }
    }

    /**
     * SelectableArrayAdapter
     *
     * @since 2021-05-25
     */
    public class SelectableArrayAdapter extends BaseItemProvider {
        private final List<Selection.Selectable> childCollection;
        /**
         * SelectableArrayAdapter
         *
         * @param collection collection
         */
        public SelectableArrayAdapter(List<Selection.Selectable> collection) {
            this.childCollection = collection;
        }

        @Override
        public int getCount() {
            return childCollection.size();
        }

        @Override
        public Object getItem(int i) {
            return childCollection.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            SingleViewHolderss singleViewHolderss;
            Component parse = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector, componentContainer, false);
            singleViewHolderss = new SingleViewHolderss(parse, i);

            final Selection.Selectable selectItem = childCollection.get(i);
            singleViewHolderss.selectprofiletitle.setText(selectItem.getName());
            loadImageToView(singleViewHolderss.imgprofilemini, selectItem);

            singleViewHolderss.rootView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    listener.onSelectableClicked(selectItem);
                    loadImageToView(singleViewHolderss.imgprofilemini, selectItem);
                }
            });
            return parse;
        }
    }

    /**
     * SingleViewHolderss
     *
     * @since 2021-05-25
     */
    protected static class SingleViewHolderss {
        private final Component rootView;
        private final CircleImageView imgprofilemini;
        private final Text selectprofiletitle;

        private SingleViewHolderss(Component convertView, int viewType) {
            super();
            rootView = convertView;
            imgprofilemini = (CircleImageView) convertView.findComponentById(ResourceTable.Id_img_profile_mini);
            selectprofiletitle = (Text) convertView.findComponentById(ResourceTable.Id_select_title);
        }
    }
}
