package com.ohos.chooseasy;

import ohos.agp.components.*;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * MultiSelectOptionsArrayAdapter
 *
 * @since 2021-06-01
 */
public class MultiSelectOptionsArrayAdapter extends BaseItemProvider {
    private static HashMap tokenObjects;
    private List collection;
    private Selection.SelectableListener listener;
    private int gridColumnsCount;

    private ComponentContainer.LayoutConfig layoutConfig;

    /**
     * MultiSelectOptionsArrayAdapter
     *
     * @param listener listener
     * @param collection collection
     * @param context context
     */
    public MultiSelectOptionsArrayAdapter(Selection.SelectableListener listener, List collection,Context context) {
        this(listener, collection, 1,context);
    }

    /**
     * MultiSelectOptionsArrayAdapter
     *
     * @param listener listener
     * @param collection collection
     * @param columnsCount int
     * @param context context
     */
    public MultiSelectOptionsArrayAdapter(Selection.SelectableListener listener, List collection, int columnsCount,Context context) {
        init(collection);

        this.listener = listener;
        gridColumnsCount = columnsCount;

        int width = getDisplayWidthInPx(context) / gridColumnsCount;
        layoutConfig = new ComponentContainer.LayoutConfig(width, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }

    /**
     * 获取屏幕宽度
     *
     * @param context 上下文
     * @return 屏幕宽度
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return (int) point.getPointX();
    }

    private void init(List<Selection.Selectable> defaultCollection) {
        collection = new ArrayList<>();
        collection.addAll(defaultCollection);

        tokenObjects = new HashMap();
    }

    /**
     * 将项目更新为选定的令牌
     *
     * @param token token
     */
    public static void updateItemToSelectedTokens(Selection.Selectable token) {
        if (token.isSelected()) {
            tokenObjects.put(token.getId(), token);
        } else {
            tokenObjects.remove(token.getId());
        }
    }

    private static void loadImageToView(Image view, Selection.Selectable selectItem) {
        if (selectItem.isSelected()) {
            view.setPixelMap(ResourceTable.Graphic_checkbox1);
        } else {
            view.setPixelMap(ResourceTable.Graphic_profile_large);
        }
    }

    @Override
    public int getCount() {
        return collection != null ? collection.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return collection.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        SelectOptionsArrayAdapter.SingleViewHolder viewHolder;
        SelectOptionsArrayAdapter.GroupViewHolder groupViewHolder1;
        int viewType = getItemViewType(i);
        if (viewType == Selection.SelectDisplayType.SINGLE.getValue() && i == Selection.SelectDisplayType.SINGLE.getValue()) {
            Component parer = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector, componentContainer, false);
            parer.setLayoutConfig(layoutConfig);
            final Selection.Selectable selectItem = (Selection.Selectable) collection.get(i);
            viewHolder = new SelectOptionsArrayAdapter.SingleViewHolder(parer, i);

            boolean isSelected = tokenObjects.containsKey(selectItem.getId());
            viewHolder.selectprofiletitle.setText(selectItem.getName());
            selectItem.setSelected(isSelected);
            loadImageToView(viewHolder.imgprofilemini, selectItem);
            viewHolder.rootView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    selectItem.setSelected(!selectItem.isSelected());
                    loadImageToView(viewHolder.imgprofilemini, selectItem);
                    updateItemToSelectedTokens(selectItem);
                    listener.onSelectableClicked(selectItem);
                }
            });

            return parer;
        } else {
            Component parer = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector_group, componentContainer, false);
            parer.setLayoutConfig(layoutConfig);
            Selection.SelectableGroup selectGroup = (Selection.SelectableGroup) collection.get(i);
            groupViewHolder1 = new SelectOptionsArrayAdapter.GroupViewHolder(parer, i);
            groupViewHolder1.selectgrouptitle.setText(selectGroup.getTitle());
            ListContainer recyclerView = groupViewHolder1.lytselectgroupcollection;
            recyclerView.setItemProvider(new MultiSelectableArrayAdapter(selectGroup.getCollection()));
            return parer;
        }
    }

    /**
     * 获取项目视图类型
     *
     * @param position int
     * @return getItemViewType
     */
    public int getItemViewType(int position) {
        if (collection.get(position) instanceof Selection.Selectable) {
            return Selection.SelectDisplayType.SINGLE.getValue();
        } else if (collection.get(position) instanceof Selection.SelectableGroup) {
            return Selection.SelectDisplayType.GROUP.getValue();
        } else {
            return 0;
        }
    }

    /**
     * MultiSelectableArrayAdapter
     *
     * @since 2021-05-25
     */
    private class MultiSelectableArrayAdapter extends BaseItemProvider {
        private final List<Selection.Selectable> childCollection;

        MultiSelectableArrayAdapter(List<Selection.Selectable> collection) {
            this.childCollection = collection;
        }

        @Override
        public int getCount() {
            return childCollection.size();
        }

        @Override
        public Object getItem(int i) {
            return childCollection.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            SelectOptionsArrayAdapter.SingleViewHolder singleViewHolder;
            Component parer = LayoutScatter.getInstance(componentContainer.getContext()).parse(ResourceTable.Layout_listitem_itemselector, componentContainer, false);
            final Selection.Selectable selectItem = childCollection.get(i);
            boolean isSelected = tokenObjects.containsKey(selectItem.getId());
            singleViewHolder = new SelectOptionsArrayAdapter.SingleViewHolder(parer, i);
            singleViewHolder.selectprofiletitle.setText(selectItem.getName());
            selectItem.setSelected(isSelected);
            loadImageToView(singleViewHolder.imgprofilemini, selectItem);

            singleViewHolder.rootView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    selectItem.setSelected(!selectItem.isSelected());
                    loadImageToView(singleViewHolder.imgprofilemini, selectItem);
                    updateItemToSelectedTokens(selectItem);
                    listener.onSelectableClicked(selectItem);
                }
            });

            return parer;
        }
    }
}