# chooseasy

## 项目介绍

- 项目名称: chooseasy
- 所属系列: openharmony的第三方组件适配移植
- 功能: 具有分组选择器的库，使选择变得容易
- 项目移植状态: 主功能完成
- 调用差异: 无
- 开发版本: sdk6，DevEco Studio 2.2 Beta1
- 基线版本：master分支


## 效果演示
<img src="img/demo1.gif"></img>

## 安装教程
无

## 使用说明
- When you need to extend and use custom objects you could implement **Selectable** Interface
All the object instances that are to be selectable (*a.k.a clickable*) should implement Selection.Selectable Interface
```java
public class Profile
        implements Serializable, Selection.Selectable {

    @SerializedName("profile_type")
    Selection.SelectedResult.IdType profileType;
    @SerializedName("profile_ref_id")
    int profileRefId;
    @SerializedName("profile_title")
    String title;
    @SerializedName("img_path")
    String profileIcon;

    @Override
    public int getId() {
        return profileRefId;
    }    
    @Override
    public String getName() {
        return title;
    }
    @Override
    public String getImage() {
        return profileIcon;
    }    
    @Override
    public void setSelected(boolean selected) {
        
    }
    @Override
    public boolean isSelected() {
        return false;
    }

}
```

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 0.0.2-SNAPSHOT

## 版权和许可信息
Open sourced with MIT license